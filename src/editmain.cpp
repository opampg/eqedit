#include"editmain.hpp"
#define tabSpaec 12

eQEdit_MainWidget::eQEdit_MainWidget(QWidget *parent):
	QWidget(parent)
{
	quit_button = new QPushButton(tr("QUIT"));
	editor = new eQTextEdit();
	editor->setTabStopWidth(tabSpaec);
	setAcceptDrops(false);

	connect(quit_button,SIGNAL(clicked()),this,SLOT(quit()));
	connect(editor,SIGNAL(doubleClick()),this,SLOT(getDoubleClicked()));
	connect(editor,SIGNAL(textChanged()),this,SLOT(chTxt()));

	QVBoxLayout* mainlayout = new QVBoxLayout();
	mainlayout->addWidget(editor);
	mainlayout->addWidget(quit_button);

	setLayout(mainlayout);
}

bool eQEdit_MainWidget::fileopen(QString filename)
{
	if(filename.isEmpty()){
		return false;
	}else{
		this->filename = filename;
	}

	QFile f(this->filename);
	if(!f.open(QFile::Text | QFile::ReadWrite)){
		return false; // fail to read.
	}
	QTextStream op(&f);
	editor->setPlainText(op.readAll());
	f.close();
	emit saveEditor();
	return true;
}

bool eQEdit_MainWidget::filesave()
{
	if(filename.isEmpty()){
//		return false;
		QString safile = QFileDialog::getSaveFileName(this,tr("Savefile"),".",tr("Text File(*)"));
		if(safile.isEmpty()){
			return false;
		}else{
			filename = safile;
		}
	}
	QFile f(filename);
	if(!f.open(QIODevice::WriteOnly)){
		return false;
	}
	QTextStream saveStream(&f);
	saveStream << editor->toPlainText();
	saveStream.flush();
	f.close();
	emit saveEditor();
	return true;
}

void eQEdit_MainWidget::getDoubleClicked(){
	this->filesave();
}

void eQEdit_MainWidget::chTxt(){
	emit changeEditor();
}


