#include<QApplication>
#include<iostream>
#include<cstdio>
#include<cstring>
#include"MainWindow.hpp"

#define version "0.0.5"

int main(int argc,char **argv)
{
	for(int n = 1;n < argc;n++){
		if(strcmp(argv[n],"--version") == 0){
			std::cout<<"version is "<<version<<std::endl;
			exit(0);
		}
	}
	QApplication eqedit(argc,argv);

	QTextCodec::setCodecForTr(QTextCodec::codecForLocale());
	QTextCodec::setCodecForCStrings(QTextCodec::codecForLocale());

	eQEdit_MainWindow *main = new eQEdit_MainWindow();
	main->show();
	
	return eqedit.exec();
}
