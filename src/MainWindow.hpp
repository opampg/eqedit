#pragma ones
#include<QMainWindow>
#include"editmain.hpp"
//#include<cstdio>

class eQEdit_MainWindow : public QMainWindow{
Q_OBJECT
public:
	eQEdit_MainWindow();

public slots:
	void setas();
	void dltas();
	void savea();

protected:
	void dragEnterEvent(QDragEnterEvent* event);
	void dropEvent(QDropEvent* event);
//	void mouseDoubleClickEvent(QMouseEvent *event){
//		printf("hello world\n");		
//	};

private:
	eQEdit_MainWidget* widget;

	QAction *save;
};
