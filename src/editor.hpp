#pragma onece
#include<QtGui>

class eQTextEdit : public QTextEdit
{
Q_OBJECT
public:
	eQTextEdit(QWidget* parent = 0);

signals:
	void doubleClick();
protected:
void mouseDoubleClickEvent(QMouseEvent *event){
	emit doubleClick();
} ;

};

