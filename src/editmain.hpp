#pragma onece
#include<QtGui>
#include"editor.hpp"
#include<cstdio>

class eQEdit_MainWidget : public QWidget{
Q_OBJECT
public:
	eQEdit_MainWidget(QWidget* parent = 0);
	bool fileopen(QString);
	bool filesave();

signals:
	void changeEditor();
	void saveEditor();

private slots:
	void quit(){exit(0);};
	void getDoubleClicked();
	void chTxt();
private:
	eQTextEdit *editor;
	QPushButton *quit_button;
	QString filename;
};
