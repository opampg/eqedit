#include"MainWindow.hpp"
#include<QtGui>

eQEdit_MainWindow::eQEdit_MainWindow():
	QMainWindow()
{
	setAcceptDrops(true);
	widget = new eQEdit_MainWidget();

	save = new QAction(tr("&Save"),this);
	save->setShortcut(tr("Ctrl+S"));

	connect(widget,SIGNAL(changeEditor()),this,SLOT(setas()));
	connect(widget,SIGNAL(saveEditor()),this,SLOT(dltas()));
	connect(save,SIGNAL(triggered()),this,SLOT(savea()));
	
	setCentralWidget(widget);
	setWindowTitle("eQEdit");
}

void eQEdit_MainWindow::dragEnterEvent(QDragEnterEvent* event)
{
	if(event->mimeData()->hasFormat("text/uri-list")){
		event->acceptProposedAction();
	}
}


void eQEdit_MainWindow::dropEvent(QDropEvent* event)
{
	QList<QUrl> urls = event->mimeData()->urls();
	if(urls.isEmpty())
		return;
	QString file_name = urls.first().toLocalFile();
	if(file_name.isEmpty())
		return;
	
	widget->fileopen(file_name);
	return;

	widget->fileopen(file_name);
}

void eQEdit_MainWindow::setas(){
	setWindowTitle("eQEdit*");
}

void eQEdit_MainWindow::dltas(){
	setWindowTitle("eQEdit");
}

void eQEdit_MainWindow::savea(){
	widget->filesave();
}
